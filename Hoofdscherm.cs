﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace SchetsEditor
{
    public class Hoofdscherm : SchetsWin
    {
        public Hoofdscherm() : base()
        {   this.ClientSize = new Size(800, 600);
            //menuStrip = new MenuStrip();
            this.Controls.Add(menuStrip);
            this.maakFileMenu();
            this.maakHelpMenu();
            this.Text = "Schets editor";
            this.Name = "SchetsEditor";
            this.Icon = Icon.FromHandle(Properties.Resources.Color_Pencils.GetHicon());
            this.FormClosing += schermClose;
        }
        private void maakFileMenu()
        {   ToolStripDropDownItem menu;
            menu = new ToolStripMenuItem("File");
            menu.DropDownItems.Add("Nieuw", null, this.nieuw);
            menu.DropDownItems.Add("Laden", null, laden);
            menu.DropDownItems.Add("Opslaan", null, opslaan);
            menu.DropDownItems.Add("Exporteer", null, export);
            menu.DropDownItems.Add("Exit", null, this.afsluiten);
            menuStrip.Items.Insert(0, menu);
        }
        private void maakHelpMenu()
        {   ToolStripDropDownItem menu;
            menu = new ToolStripMenuItem("Help");
            menu.DropDownItems.Add("Over \"SchetsPlusPlus\"", null, this.about);
            menuStrip.Items.Add(menu);
        }
        private void about(object o, EventArgs ea)
        {   MessageBox.Show("Schets versie 1.1\n(c) UU Informatica 2010\n" +
                                "(c) Martijn Wester & Sophie Huiberts 2012\n" +
                                "Broncode op " +
                                "https://bitbucket.org/shuiberts/schetsplusplus"
                           , "Over \"SchetsPlusPlus\""
                           , MessageBoxButtons.OK
                           , MessageBoxIcon.Information
                           );
        }

        private void nieuw(object sender, EventArgs e)
        {   (new Thread( () => {
                Application.Run(new Hoofdscherm());})).Start();
        }
        private void laden(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Any file|*|JPEG Image|*.jpg;*.jpeg|BMP Bitmap|*.bmp|" +
                "PNG Image|*.png|TIFF|*.tiff|GIF|*.gif|Windows Icon|*.ico|" +
                    "SVG Vector graphics|*.svg";
            ofd.FileOk += delegate {
                schetscontrol.Laad(ofd.FileName);
                changed = false;
            };
            ofd.ShowDialog();
        }
        private void opslaan(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Vector Graphics (SVG)|*.svg";
            sfd.FileOk += delegate {
                schetscontrol.schets.Save(sfd.FileName);
            };
            sfd.ShowDialog();
        }
        private void export(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "JPEG Image|*.jpg;*.jpeg|BMP Bitmap|*.bmp|" +
                "PNG Image|*.png|TIFF|*.tiff|GIF|*.gif|Windows Icon|*.ico";
            sfd.FileOk += delegate {
                System.Drawing.Imaging.ImageFormat format;
                switch(sfd.FilterIndex) {
                    case 1: // JPEG
                        format = System.Drawing.Imaging.ImageFormat.Jpeg;
                        break;
                    case 2: // BMP
                        format = System.Drawing.Imaging.ImageFormat.Bmp;
                        break;
                    default:
                    case 3: // PNG
                        format = System.Drawing.Imaging.ImageFormat.Png;
                        break;
                    case 4: // TIFF
                        format = System.Drawing.Imaging.ImageFormat.Tiff;
                        break;
                    case 5: // GIF
                        format = System.Drawing.Imaging.ImageFormat.Gif;
                        break;
                    case 6: // Windows Icon
                        format = System.Drawing.Imaging.ImageFormat.Icon;
                        break;
                }
                schetscontrol.schets.image.Save(sfd.FileName, format);
                changed = false;
            };
            sfd.ShowDialog();
        }

        private void afsluiten(object sender, EventArgs e)
        {   this.Close();
            schermClose(sender, e);
        }


        private void schermClose(object o, EventArgs e)
        {
            if(changed)//if schets is changed
            if(MessageBox.Show("Changes were made, Save?",
                                   "Do you want to save?",
                                   MessageBoxButtons.YesNo) == DialogResult.Yes)
                opslaan(null, null);
        }
    }
}
