﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Resources;


namespace SchetsEditor
{
    public class SchetsWin : Form
    {   
        protected MenuStrip menuStrip;
        public SchetsControl schetscontrol;
        public PictureBox colorBox1 = new PictureBox();
        public PictureBox colorBox2 = new PictureBox();
        ISchetsTool huidigeTool = new PenStreep();
        Label lblPenSize; TrackBar sizeBar = new TrackBar();
        protected bool changed = false;

        public TrackBar SizeBar
        {
            get
            {
                return sizeBar;
            }
        }

        public ISchetsTool HuidigeTool
        {
            get
            {
                return huidigeTool;
            }
        }

        Panel paneel;
        bool vast;
        ResourceManager resourcemanager
            = new ResourceManager("SchetsEditor.Properties.Resources"
                                 , Assembly.GetExecutingAssembly()
                                 );

        protected void veranderAfmeting(object o, EventArgs ea)
        {
            schetscontrol.Size = new Size ( this.ClientSize.Width  - 70
                                          , this.ClientSize.Height - 90);
            schetscontrol.Location = new Point(64, 90);
            paneel.Location = new Point(64, 30);
        }

        private void klikToolMenu(object obj, EventArgs ea)
        {
            this.huidigeTool = (ISchetsTool)((ToolStripMenuItem)obj).Tag;
            for (int i = 1; i < 10; i++)
            {
                if (this.Controls[i].Tag.ToString() == ((ISchetsTool)((ToolStripMenuItem)obj).Tag).ToString())
                    ((RadioButton)this.Controls[i]).Checked = true;
                else
                    ((RadioButton)this.Controls[i]).Checked = false;
            }
            huidigeTool.PenSize = sizeBar.Value;            
        }

        private void klikToolButton(object obj, EventArgs ea)
        {
            this.huidigeTool = (ISchetsTool)((RadioButton)obj).Tag;
            huidigeTool.PenSize = sizeBar.Value;
        }

        private void afsluiten(object obj, EventArgs ea)
        {
            this.Close();
        }

        public SchetsWin()
        {
            ISchetsTool[] deTools = { new PenStreep()         
                                    , new Lijn()
                                    , new Rechthoek()
                                    , new Vlak()
                                    , new Cirkel()
                                    , new VolCirkel()
                                    , new Tekst()
                                    , new Gum()
                                    , new PickColor()
                                    };
            String[] deKleuren = { "Black", "Red", "Green", "Blue"
                                 , "Yellow", "Magenta", "Cyan" 
                                 };

            this.ClientSize = new Size(700, 500);

            schetscontrol = new SchetsControl();
            schetscontrol.Location = new Point(64, 30);
            schetscontrol.MouseDown += (object o, MouseEventArgs mea) =>
            {
                changed = true;
                vast=true;
                if(mea.Button == MouseButtons.Left)
                    huidigeTool.Kleur = schetscontrol.penkleur1;
                else
                    huidigeTool.Kleur = schetscontrol.penkleur2;
                huidigeTool.MuisVast(schetscontrol, mea.Location, mea); 
            };
            schetscontrol.MouseMove += (object o, MouseEventArgs mea) =>
            {
                if (vast)
                    huidigeTool.MuisDrag(schetscontrol, mea.Location, mea); 
            };
            schetscontrol.MouseClick += (object o, MouseEventArgs mea) =>
            {
                huidigeTool.MuisLos(schetscontrol, new Point(mea.Location.X + 1, mea.Location.Y), mea);
            };
            schetscontrol.MouseUp   += (object o, MouseEventArgs mea) =>
            {
                vast=false;
                if(mea.Button == MouseButtons.Left)
                    huidigeTool.Kleur = schetscontrol.penkleur1;
                else
                    huidigeTool.Kleur = schetscontrol.penkleur2;
                huidigeTool.MuisLos(schetscontrol, mea.Location, mea);

                schetscontrol.schets.VoegToe(huidigeTool);
                huidigeTool = huidigeTool.Nieuw();
            };
            schetscontrol.KeyPress +=  (object o, KeyPressEventArgs kpea) => 
            {
                schetscontrol.schets.elementen.Last.Value.Letter(schetscontrol, kpea.KeyChar);
                schetscontrol.schets.Herteken();
            };
            this.Controls.Add(schetscontrol);

            menuStrip = new MenuStrip();
            menuStrip.Visible = true;
            this.Controls.Add(menuStrip);
            this.maakAktieMenu(deKleuren);
            this.maakToolMenu(deTools);
            this.maakToolButtons(deTools);
            this.maakAktieButtons(deKleuren);
            this.Resize += this.veranderAfmeting;
            this.veranderAfmeting(null, null);
        }

        private void maakToolMenu(ICollection<ISchetsTool> tools)
        {   
            ToolStripMenuItem menu = new ToolStripMenuItem("Tool");
            foreach (ISchetsTool tool in tools)
            {   ToolStripItem item = new ToolStripMenuItem();
                item.Tag = tool;
                item.Text = tool.ToString();
                item.Image = (Image)resourcemanager.GetObject(tool.ToString());
                item.Click += this.klikToolMenu;
                menu.DropDownItems.Add(item);
            }
            menuStrip.Items.Add(menu);
        }

        private void maakAktieMenu(String[] kleuren)
        {   
            ToolStripMenuItem menu = new ToolStripMenuItem("Action");
            menu.DropDownItems.Add("Clear", null, schetscontrol.Schoon );
            menu.DropDownItems.Add("Roteer", null, schetscontrol.Roteer );
            menu.DropDownItems.Add("Edit Colors", null, schetscontrol.PickColor);
            menuStrip.Items.Add(menu);
        }

        private void maakToolButtons(ICollection<ISchetsTool> tools)
        {
            int t = 0;
            foreach (ISchetsTool tool in tools)
            {
                RadioButton b = new RadioButton();
                b.Appearance = Appearance.Button;
                b.Size = new Size(65, 62);
                b.Location = new Point(0, 30 + t * 62);
                b.Tag = tool;
                b.Text = tool.ToString();
                b.Font = new Font("Arial Narrow", 7, FontStyle.Bold);
                b.Image = (Image)resourcemanager.GetObject(tool.ToString());
                b.TextAlign = ContentAlignment.TopCenter;
                b.ImageAlign = ContentAlignment.BottomCenter;
                b.Click += this.klikToolButton;
                this.Controls.Add(b);
                if (t == 0) b.Select();
                t++;
            }
        }

        private void maakAktieButtons(String[] kleuren)
        {   
            paneel = new Panel();
            paneel.Size = new Size(580, 62);
            paneel.Name = "paneel";
            this.Controls.Add(paneel);

            Button b; Label l;
            b = new Button(); 
            b.Text = "Clear";  
            b.Location = new Point(  10, 3); 
            b.Click += schetscontrol.Schoon; 
            paneel.Controls.Add(b);
            
            b = new Button(); 
            b.Text = "Rotate"; 
            b.Location = new Point( 10, 33); 
            b.Click += schetscontrol.Roteer; 
            paneel.Controls.Add(b);

            l = new Label();
            l.Font = new Font("Arial", 12);
            l.Text = "Colors:";
            l.Location = new Point(118, 3);
            l.AutoSize = true;
            paneel.Controls.Add(l);

            b = new Button();
            b.Text = "Set Colors";
            b.Location = new Point(200, 3);
            b.Click += schetscontrol.PickColor;
            paneel.Controls.Add(b);

            schetscontrol.penkleur1 = Color.Black;
            schetscontrol.penkleur2 = Color.White;

            l = new Label();
            l.Font = new Font("Arial", 10);
            l.Text = "L:";
            l.Location = new Point(280, 5);
            l.AutoSize = true;
            paneel.Controls.Add(l);

            colorBox1.Location = new Point(305, 3);
            colorBox1.Size = new Size(20, 20);
            colorBox1.Image = schetscontrol.borderLines(colorBox1.Size);
            colorBox1.BackColor = schetscontrol.penkleur1;
            paneel.Controls.Add(colorBox1);

            l = new Label();
            l.Font = new Font("Arial", 10);
            l.Text = "R:";
            l.Location = new Point(330, 5);
            l.AutoSize = true;
            paneel.Controls.Add(l);

            colorBox2.Location = new Point(355, 3);
            colorBox2.Size = new Size(20, 20);
            colorBox2.Image = schetscontrol.borderLines(colorBox2.Size);
            colorBox2.BackColor = schetscontrol.penkleur2;
            paneel.Controls.Add(colorBox2);

            Color[] standardColors = { Color.Black, Color.Gray, Color.Red, Color.Yellow, Color.FromArgb(0, 255, 0), Color.Cyan,
                                     Color.White, Color.Gainsboro, Color.Blue, Color.Magenta, Color.FromArgb(255, 255, 128), Color.FromArgb(0, 255, 128)};

            for (int j = 0; j < 2; j++)
            for (int i = 0; i < 6; i++)
            {
                PictureBox kleurBox = new PictureBox();
                kleurBox.Location = new Point(420 + 25*i, 3 + 25 * j);
                kleurBox.Size = new Size(20, 20);
                kleurBox.Image = schetscontrol.borderLines(colorBox1.Size);
                kleurBox.BackColor = standardColors[i + 6*j];
                paneel.Controls.Add(kleurBox);
                kleurBox.MouseClick += new MouseEventHandler(kleurBox_MouseClick);
            }


            sizeBar = new TrackBar();
            sizeBar.Location = new Point(170, 30);
            sizeBar.Size = new Size(180, 24);
            sizeBar.TickStyle = TickStyle.None;
            sizeBar.Minimum = 1;
            sizeBar.Maximum = 20;
            sizeBar.Value = 4;
            paneel.Controls.Add(sizeBar);
            huidigeTool.PenSize = sizeBar.Value;
            sizeBar.ValueChanged += new EventHandler(size_Changed);

            Label l2 = new Label();
            l2.Font = new Font("Arial", 12);
            l2.Text = "Size:";
            l2.Location = new Point(118, 30);
            l2.AutoSize = true;
            paneel.Controls.Add(l2);

            lblPenSize = new Label();
            lblPenSize.Font = new Font("Arial", 10);
            lblPenSize.Text = "(" + sizeBar.Value + ")";
            lblPenSize.Location = new Point(350, 31);
            lblPenSize.AutoSize = true;
            paneel.Controls.Add(lblPenSize);
        }

        void kleurBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            schetscontrol.set_Color(sender, e, 1, ((PictureBox)sender).BackColor);
            else
                schetscontrol.set_Color(sender, e, 2, ((PictureBox)sender).BackColor);
        }

        void size_Changed(object sender, EventArgs e)
        {
            huidigeTool.PenSize = ((TrackBar)sender).Value;
            lblPenSize.Text = "(" + ((TrackBar)sender).Value + ")";
            //Check if size is changed while cursor is still on paintscreen
            Point p = PointToScreen(schetscontrol.Location);
            if ((MousePosition.X > p.X + 1 && MousePosition.X < p.X - 2 + schetscontrol.Width)
                    && (MousePosition.Y > p.Y + 1 && MousePosition.Y < p.Y - 2 + schetscontrol.Height))
                schetscontrol.changeCursor(sender, e);
           
        }
    }
}
