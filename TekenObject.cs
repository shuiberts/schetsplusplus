using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Windows.Forms;

namespace SchetsEditor
{
    public interface ISchetsTool
    {
        void MuisVast(SchetsControl s, Point p, MouseEventArgs mea);
        void MuisDrag(SchetsControl s, Point p, MouseEventArgs mea);
        void MuisLos(SchetsControl s, Point p, MouseEventArgs mea);
        void Teken(Graphics g);
        void Letter(SchetsControl s, char c);
        void Roteer(int x, int y, int alpha);
        bool Bevat(Point p);
        String ToSVG();
        ISchetsTool Nieuw();
        int PenSize {get; set;}
        Color Kleur {get; set;}
    }

    public abstract class Startpunt : ISchetsTool
    {
        protected Color kleur = Color.Black;
        protected int pensize = 4;
        
        public int PenSize {
            get {
                return pensize;
            }
            set {
                pensize = value;
            }
        }
        public Color Kleur {
            get {
                return kleur;
            }
            set {
                kleur = value;
                kwast = new SolidBrush(kleur);
            }
        }

        public Point startpunt;
        public Brush kwast;

        public override String ToString() {
            return this.GetType().Name;
        }
        public ISchetsTool Nieuw() {
            Startpunt o = (Startpunt)Activator.CreateInstance(this.GetType());
            o.PenSize = pensize;
            o.kwast = kwast;
            o.startpunt = startpunt;
            return o;
        }

        public virtual void MuisVast(SchetsControl s, Point p, MouseEventArgs mea)
        {
            startpunt = p;
        }
        public virtual void MuisLos(SchetsControl s, Point p, MouseEventArgs mea) {}

        // Roteer 90 graden, wees geroteerd over alpha graden (met alpha = n*90)
        public virtual void Roteer(int x, int y, int alpha) {
            Point tmp = new Point();
            tmp.X = (y - startpunt.Y) + x;
            tmp.Y = (startpunt.X - x) + y;
            startpunt = tmp;
        }

        public abstract void MuisDrag(SchetsControl s, Point p, MouseEventArgs mea);
        public abstract void Letter(SchetsControl s, char c);
        public abstract void Teken(Graphics g);
        public abstract bool Bevat(Point p);
        public abstract String ToSVG();


        /*
         * Parses all xml parameters, and expands css arguments in style='s
         * Input: a piece of xml representing one svg element
         * Output: a Dictionary<string, string> indexed with the names
         * of the xml parameters, filled with the contents of them.
         * Plus, the style= will be expanded and placed in their
         * corresponding places in the dictionary.
         * The "Contents" key is for this like <tag>this</tag>
         */
        public static Dictionary<string, string> SVGToDict(string s)
        {
            Dictionary<string, string> result= new Dictionary<string, string>();

            // Removing all the meanies
            s = s.Replace('\'', '"');
            s = s.Replace('\n', ' ');
            s = s.Replace('\r', ' ');
            s = s.Replace('\t', ' ');
            while(s.Contains("  "))
                s = s.Replace("  ", " ");

            // Removing the < and />
            s = s.TrimStart('<');
            s = s.TrimEnd('>');
            s = s.TrimEnd('/');

            // Getting eventual contents of xml tag (like <tag>this here</tag>
            if(s.Contains("<")) {
                string afterclosing = s.Substring(s.IndexOf(">") + 1);
                s = s.Substring(0, s.IndexOf(">"));

                string beforeopening = afterclosing.Substring(0, afterclosing.IndexOf("<"));
                result.Add("Contents", beforeopening);
            }

            /*
             * Sorry voor de lelijkheid, maar strings parsen is niet bepaald
             * mijn sterkste punt. Is er een college regex?
             */
            string[] pars = s.Split(' ');
            foreach(String par in pars) {
                string[] parts = par.Split('=');
                if(parts[0]=="style") {
                    string[] styles = parts[1].Trim('"').Split(';');
                    foreach(string style in styles) {
                        if(style != "") {
                            string[] styleparts = style.Split(':');
                            if(styleparts.Length == 2)
                                result.Add(styleparts[0].ToLower(), styleparts[1]);
                            else
                                Console.WriteLine("Could not parse style:" + style);
                        }
                    }
                } else if(parts.Length == 2)
                    result.Add(parts[0].ToLower(), parts[1].Substring(1, parts[1].Length - 2));
                else if(par != pars[0])
                    Console.WriteLine("Could not parse:" + par);
            }

            return result;
        }

        public Color CSSArgToColor(string s) {
            // rgba( is not supported, as it is not standard SVG anyway
            if(s.StartsWith("rgb(")) {
                s = s.Trim(new char[]{'r', 'g', 'b', '(', ')', ';', ' '});

                string[] colors = s.Split(',');
                int r = int.Parse(colors[0].Trim());
                int g = int.Parse(colors[1].Trim());
                int b = int.Parse(colors[2].Trim());
                return Color.FromArgb(r, g, b);
            }
            return Color.FromName(s);
        }
    }

    public class Tekst : Startpunt
    {
        String tekst = "";
        float width;
        int rotatie = 0;

        public Tekst() {}
        public Tekst(string svg)
        {
            Dictionary<string, string> pars = SVGToDict(svg);

            startpunt = new Point(int.Parse(pars["x"]),
                                  int.Parse(pars["y"]));

            kwast = new SolidBrush(CSSArgToColor(pars["fill"]));
            try {
                pensize = int.Parse(pars["stroke-width"]);
            } catch {
                pensize = 4;
            }

            tekst = pars["Contents"];

            string transformation = pars["transform"];
            string rotationstr = transformation.Substring(transformation.IndexOf("rotate("));

            // Remove closing ) and everything behind it
            rotationstr = rotationstr.Substring(0, rotationstr.IndexOf(")"));

            // 7 is the amount of characters in "rotate("
            rotatie = int.Parse(rotationstr.Substring(7));
        }

        public override void MuisVast(SchetsControl s, Point p, MouseEventArgs mea) {
            base.MuisVast(s, p, mea);
        }

        public override void MuisDrag(SchetsControl s, Point p, MouseEventArgs mea) { }

        public override void Teken(Graphics g)
        {
            Font font = new Font("Tahoma", 2*PenSize);
            g.TranslateTransform(startpunt.X, startpunt.Y);
            g.RotateTransform(rotatie);
            g.TranslateTransform(-startpunt.X, -startpunt.Y);
            g.DrawString   (tekst, font, kwast, 
                                              this.startpunt, StringFormat.GenericTypographic);
            width = g.MeasureString(tekst, font).Width;
        }

        public override void Letter(SchetsControl s, char c)
        {
            if (c >= 32)
            {
                Graphics gr = s.MaakBitmapGraphics();
                gr.SmoothingMode = SmoothingMode.None;
                tekst += c.ToString();
                Teken(gr);
                s.Invalidate();
            }
        }
        public override bool Bevat(Point p) {
            return p.Y > startpunt.Y &&
                   p.Y < startpunt.Y + 2*pensize &&
                   p.X > startpunt.X &&
                   p.X < startpunt.X + width;
        }
        public override void Roteer(int x, int y, int alpha) {
            base.Roteer(x, y, alpha);
            rotatie = alpha;
        }
        public override string ToSVG()
        {
            return "<text " +
                "x=\"" + startpunt.X + "\" " +
                "y=\"" + startpunt.Y + "\" " +
                "fill=\"rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B + ");\" " +
                "transform=\"rotate(" + rotatie + ")\">" +
                    tekst +
                "</text>\n";
        }
    }

    public abstract class Tweepunt : Startpunt
    {
        protected Point eindpunt;

        public static Rectangle Punten2Rechthoek(Point p1, Point p2)
        {   return new Rectangle( new Point(Math.Min(p1.X,p2.X), Math.Min(p1.Y,p2.Y))
                                , new Size (Math.Abs(p1.X-p2.X), Math.Abs(p1.Y-p2.Y))
                                );
        }
        public static Pen MaakPen(Brush b, int dikte)
        {   Pen pen = new Pen(b, dikte);
            pen.StartCap = LineCap.Round;
            pen.EndCap = LineCap.Round;
            return pen;
        } 
        public override void MuisVast(SchetsControl s, Point p, MouseEventArgs mea)
        {   base.MuisVast(s, p, mea);
        }
        public override void MuisDrag(SchetsControl s, Point p, MouseEventArgs mea)
        {   s.Refresh();
        this.Bezig(s.CreateGraphics(), this.startpunt, p);
        }
        public override void MuisLos(SchetsControl s, Point p, MouseEventArgs mea)
        {   base.MuisLos(s, p, mea);
            this.Compleet(s.MaakBitmapGraphics(), this.startpunt, p);
            s.Refresh();
            eindpunt = p;
        }
        public override void Letter(SchetsControl s, char c)
        {
        }
        public override void Roteer(int x, int y, int alpha) {
            Point tmp = new Point();
            tmp.X = (y - eindpunt.Y) + x;
            tmp.Y = (eindpunt.X - x) + y;
            eindpunt = tmp;
            base.Roteer(x, y, alpha);
        }

        public abstract void Bezig(Graphics g, Point p1, Point p2);

        public virtual void Compleet(Graphics g, Point p1, Point p2)
        {
            this.Bezig(g, p1, p2);
        }

        public static double Afstand(Point p1, Point p2) {
            double dx = p1.X - p2.X;
            double dy = p1.Y - p2.Y;
            return Math.Sqrt(dx*dx + dy*dy);
        }
        public static Point Midden(Point p1, Point p2) {
            return new Point((p1.X + p2.X)/2, (p1.Y + p2.Y)/2);
        }
    }

    public class Rechthoek : Tweepunt
    {
        public Rechthoek() {}
        public Rechthoek(string svg)
        {
            Dictionary<string, string> pars = SVGToDict(svg);

            startpunt = new Point(int.Parse(pars["x"]),
                                  int.Parse(pars["y"]));
            eindpunt = new Point(startpunt.X + int.Parse(pars["width"]),
                                 startpunt.Y + int.Parse(pars["height"]));
            kwast = new SolidBrush(CSSArgToColor(pars["stroke"]));
            try {
                pensize = int.Parse(pars["stroke-width"]);
            } catch {
                pensize = 4;
            }
        }

        public override void Teken(Graphics g) {
            g.DrawRectangle(MaakPen(kwast, pensize),
                            Tweepunt.Punten2Rechthoek(startpunt, eindpunt));
        }

        public override void Bezig(Graphics g, Point p1, Point p2)
        {   g.DrawRectangle(MaakPen(kwast,pensize),
                                Tweepunt.Punten2Rechthoek(p1, p2));
        }
        
        public override bool Bevat(Point p) {
            Rectangle rect = Punten2Rechthoek(startpunt, eindpunt);
            Rectangle outer = new Rectangle(rect.Left - pensize,
                                            rect.Top - pensize,
                                            rect.Width + 2*pensize,
                                            rect.Height + 2*pensize);
            Rectangle inner = new Rectangle(rect.Left + pensize,
                                            rect.Top + pensize,
                                            rect.Width - 2*pensize,
                                            rect.Height - 2*pensize);
            return outer.Contains(p) && !inner.Contains(p);
        }
        public override string ToSVG()
        {

            Rectangle rect = Punten2Rechthoek(startpunt, eindpunt);

            return "<rect " + 
                "x=\"" + rect.X + "\" "  +
                "y=\"" + rect.Y + "\" "  +
                "width=\"" + rect.Width + "\" " +
                "height=\"" + rect.Height + "\" " +
                    "style=\"fill-opacity:0;" +
                "stroke:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                    ");" +
                "\"/>\n";
        }
    }
    
    public class Vlak : Rechthoek
    {
        public Vlak() {}
        public Vlak(string svg) : base(svg) {}

        public override void Teken(Graphics g)
        {
            g.FillRectangle(kwast, Tweepunt.Punten2Rechthoek(startpunt, eindpunt));
        }
        public override void Compleet(Graphics g, Point p1, Point p2)
        {   g.FillRectangle(kwast, Tweepunt.Punten2Rechthoek(p1, p2));
        }
        public override bool Bevat(Point p) {
            Rectangle rect = Punten2Rechthoek(startpunt, eindpunt);
            return rect.Contains(p);
        }
        public override string ToSVG()
        {
            Rectangle rect = Punten2Rechthoek(startpunt, eindpunt);

            return "<rect " + 
                "x=\"" + rect.X + "\" "  +
                "y=\"" + rect.Y + "\" "  +
                "width=\"" + rect.Width + "\" " +
                "height=\"" + rect.Height + "\" " +
                "style=\"fill:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                    ");" +
                "stroke:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                    ");" +
                "\"/>\n";
        }
    }

    public class Cirkel : Tweepunt
    {
        public Cirkel() {}
        public Cirkel(string svg)
        {
            Dictionary<string, string> pars = SVGToDict(svg);
            
            int cx = int.Parse(pars["cx"]);
            int cy = int.Parse(pars["cy"]);
            int rx = int.Parse(pars["rx"]);
            int ry = int.Parse(pars["ry"]);
            startpunt = new Point(cx - rx, cy - ry);
            eindpunt = new Point(cx + rx, cy + ry);
            kwast = new SolidBrush(CSSArgToColor(pars["stroke"]));
            try {
                pensize = int.Parse(pars["stroke-width"]);
            } catch {
                pensize = 4;
            }
        }

        public override void MuisLos(SchetsControl s, Point p, MouseEventArgs mea) {
            base.MuisLos(s, p, mea);
        }
        public override void Bezig(Graphics g, Point p1, Point p2)
        {
            g.DrawEllipse(MaakPen(kwast, pensize), Tweepunt.Punten2Rechthoek(p1, p2));
        }
        public override void Teken(Graphics g) {
            g.DrawEllipse(MaakPen(kwast, pensize), Tweepunt.Punten2Rechthoek(startpunt, eindpunt));
        }
        public override bool Bevat(Point p) {
            Point m = Midden(startpunt, eindpunt);
            double radius = Afstand(startpunt, m);

            // Deling door 0 voorkomen
            if(startpunt.Y == eindpunt.Y)
                return Math.Abs(startpunt.Y - p.Y) < pensize &&
                    Afstand(p, m) <= radius;

            // Sorry voor de berekeningen, maar ellipsen zijn nu eenmaal niet leuk
            // afstandtotmidden is de afstand als de ruimte zo wordt geschaald
            // dat de ellips een cirkel is.
            double afstandtotmidden = Afstand(new Point(p.X - m.X, (p.Y - m.Y)*
                                                        (eindpunt.X - startpunt.X)/
                                                        (eindpunt.Y - startpunt.Y)),
                                              new Point(0, 0));
            return afstandtotmidden > radius - pensize &&
                afstandtotmidden < radius + pensize;
        }
        public override string ToSVG()
        {
            
            Rectangle rect = Punten2Rechthoek(startpunt, eindpunt);

            return "<ellipse " + 
                "cx=\"" + (rect.X + rect.Width/2) + "\" "  +
                "cy=\"" + (rect.Y + rect.Height/2) + "\" "  +
                "rx=\"" + (rect.Width/2) + "\" " +
                "ry=\"" + (rect.Height/2) + "\" " +
                    "style=\"fill-opacity:0;" +
                "stroke:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                    ");stoke-width:" + pensize +
                "\"/>\n";
        }
    }

    public class VolCirkel : Cirkel
    {
        public VolCirkel() {}
        public VolCirkel(string s) : base(s) {}

        public override void Compleet(Graphics g, Point p1, Point p2)
        {
            g.FillEllipse(kwast, Tweepunt.Punten2Rechthoek(p1, p2));
        }
        public override void Teken(Graphics g) {
            g.FillEllipse(kwast, Tweepunt.Punten2Rechthoek(startpunt, eindpunt));
        }
        public override bool Bevat(Point p) {
            Point m = Midden(startpunt, eindpunt);
            double radius = Afstand(startpunt, m);

            // Deling door 0 voorkomen
            if(startpunt.Y == eindpunt.Y)
                return Math.Abs(startpunt.Y - p.Y) < pensize &&
                    Afstand(p, m) <= radius;

            // Sorry voor de berekeningen, maar ellipsen zijn nu eenmaal niet leuk
            // afstandtotmidden is de afstand als de ruimte zo wordt geschaald
            // dat de ellips een cirkel is.
            double afstandtotmidden = Afstand(new Point(p.X - m.X, (p.Y - m.Y)*
                                                        (eindpunt.X - startpunt.X)/
                                                        (eindpunt.Y - startpunt.Y)),
                                              new Point(0, 0));
            return afstandtotmidden <= radius;
        }
        public override string ToSVG()
        {
            Rectangle rect = Punten2Rechthoek(startpunt, eindpunt);

            return "<ellipse " + 
                "cx=\"" + (rect.X + rect.Width/2) + "\" "  +
                "cy=\"" + (rect.Y + rect.Height/2) + "\" "  +
                "rx=\"" + rect.Width/2 + "\" " +
                "ry=\"" + rect.Height/2 + "\" " +
                "style=\"fill:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                    ");" +
                "stroke:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                    ");" +
                "\"/>\n";
        }
    }

    public class Lijn : Tweepunt
    {
        public Lijn() {}
        public Lijn(string svg)
        {
            Dictionary<string, string> pars = SVGToDict(svg);

            startpunt = new Point(int.Parse(pars["x1"]),
                                  int.Parse(pars["y1"]));
            eindpunt = new Point(int.Parse(pars["x2"]),
                                 int.Parse(pars["y2"]));
            kwast = new SolidBrush(CSSArgToColor(pars["stroke"]));
            try {
                pensize = int.Parse(pars["stroke-width"]);
            } catch {
                pensize = 4;
            }
        }

        public override void Bezig(Graphics g, Point p1, Point p2)
        {   g.DrawLine(MaakPen(this.kwast,pensize), p1.X, p1.Y, p2.X, p2.Y);
        }
        public override void Teken(Graphics g) {
            g.DrawLine(MaakPen(this.kwast,pensize), startpunt, eindpunt);
        }
        public override bool Bevat(Point p) {
            return AfstandPuntTotLijn(p, startpunt, eindpunt) < pensize;
        }
        public static double AfstandPuntTotLijn(Point p, Point p1, Point p2) {
            // Als het eigenlijk een punt is
            if(p1.X == p2.X && p1.Y == p2.Y)
                return Afstand(p, p1);

            /* Stel de lijn voor als
             * ax + by + c = 0
             * Dat is met een factor sqrt(a*a + b*b) analoog
             * aan de afstand van een punt (x,y) tot die lijn.
             * Voor het gemak herschrijven we de formule
             * van de lijn naar
             * a(x + X) + b(y + Y) = 0
             * voor het punt (x, y), richtingscoefficient a/b
             * en steunvector (X, Y).
             * In de toewijzing van afstand wordt een keer
             * p2 - p1 en een keer p1 - p2
             * gebruikt. Dat is omdat de y as de andere kant op
             * loopt dan normaal, maar de x as wel goed loopt.
             */
            double lengte = Tweepunt.Afstand(p1, p2);
            double afstand = Math.Abs(((p2.X - p1.X)*(p2.Y - p.Y) + 
                              (p1.Y - p2.Y)*(p2.X - p.X)) / lengte);

            /* Nu moeten we nog controlleren, of de afstand
             * die we gevonden hebben wel de afstand is tot het
             * lijnsegment, of de afstand tot het verlengde ervan.
             * Maar we weten al, dat de afstand tot het verlengde
             * verwaarloosbaar klein is, en dat verwaarlozen we dus
             * ook. Dan kunnen we gewoon kijken naar de afstand
             * tot de p2en van de lijn.
             */
            double d1 = Afstand(p1, p);
            double d2 = Afstand(p2, p);
            if(Math.Max(d1, d2) > lengte)
                return Math.Min(d1, d2);

            return afstand;
        }
        public override string ToSVG()
        {
            return "<line " +
                "x1=\"" + startpunt.X + "\" " +
                "y1=\"" + startpunt.Y + "\" " +
                "x2=\"" + eindpunt.X + "\" " +
                "y2=\"" + eindpunt.Y + "\" " +
                "style=\"stroke:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                ");" +
                "stroke-width:" + pensize + "\"/>\n";
        }
    }

    public class PenStreep : Lijn
    {
        public PenStreep() {}
        public PenStreep(string svg)
        {
            Dictionary<string, string> pars = SVGToDict(svg);

            kwast = new SolidBrush(CSSArgToColor(pars["stroke"]));
            try {
                pensize = int.Parse(pars["stroke-width"]);
            } catch {
                pensize = 4;
            }

            string pad = pars["d"];
            string puntenlijst = pad.Substring(pad.IndexOf('L') + 1).Trim();

            string[] coordinaten = puntenlijst.Split(',');

            for(int i = 0; i <= coordinaten.Length; i += 2) {
                if(coordinaten[i] != "" && coordinaten[i+1] != "")
                    punten.AddLast(new Point(int.Parse(coordinaten[i]),
                                             int.Parse(coordinaten[i+1])));
            }
        }

        LinkedList<Point> punten = new LinkedList<Point>();

        public override void MuisVast(SchetsControl s, Point p, MouseEventArgs mea)
        {
            punten.AddLast(p);
            base.MuisVast(s, p, mea);
        }
        public override void MuisDrag(SchetsControl s, Point p, MouseEventArgs mea)
        {   this.MuisLos(s, p, mea);
            this.MuisVast(s, p, mea);
        }
        public override void Teken(Graphics g)
        {
            LinkedListNode<Point> p1 = punten.First;
            LinkedListNode<Point> p2 = p1.Next;
            g.DrawLine(MaakPen(kwast, pensize), p1.Value, p1.Value);
            while(p2 != null) {
                g.DrawLine(MaakPen(kwast, pensize), p1.Value, p2.Value);
                p1 = p2;
                p2 = p1.Next;
            }
        }
        
        public override bool Bevat(Point p) {
            LinkedListNode<Point> p1 = punten.First;
            LinkedListNode<Point> p2 = p1.Next;
            while(p2 != null) {
                if(AfstandPuntTotLijn(p, p1.Value, p2.Value) < pensize)
                    return true;
                p1 = p2;
                p2 = p1.Next;
            }
            return false;
        }
        public override string ToSVG()
        {
            LinkedListNode<Point> node = punten.First;
            String resultaat = "<path d=\"M" + node.Value.X + "," + node.Value.Y +
                "L" + node.Value.X + "," + node.Value.Y + ",";
            while((node = node.Next) != null) {
                resultaat += node.Value.X + "," + node.Value.Y + ",";
            }
            resultaat += "\" fill=\"none\" " +
                "stroke-width=\"" + pensize +
                "\" style=\"stroke:rgb(" +
                    kleur.R + "," +
                    kleur.G + "," +
                    kleur.B +
                ");\" stroke-linecap=\"round\" " +
                "stroke-linejoin=\"round\"/>\n";
            return resultaat;
        }
        public override void Roteer(int x, int y, int alpha) {
            LinkedListNode<Point> node = punten.First;
            do {
                Point tmp = new Point();
                tmp.X = (y - node.Value.Y) + x;
                tmp.Y = (node.Value.X - x) + y;
                node.Value = tmp;
            } while((node = node.Next) != null);
        }
    }

    public class Gum : ISchetsTool
    {
        protected int pensize;
        public int PenSize {
            get { return pensize; }
            set { pensize = value; }
        }
        public Color Kleur {
            get { return Color.White; }
            set {}
        }

        public override string ToString()
        {
            return "Gum";
        }

        public void MuisVast(SchetsControl s, Point p, MouseEventArgs mea) {
            MuisDrag(s, p, mea);
        }
        public void MuisLos(SchetsControl s, Point p, MouseEventArgs mea) {}
        public void Letter(SchetsControl s, char c) {}
        public void Teken(Graphics g) { }
        public bool Bevat(Point p) {return false;}
        public void Roteer(int x, int y, int alpha) {}

        public ISchetsTool Nieuw() {
            return this;
        }

        public void MuisDrag(SchetsControl s, Point p, MouseEventArgs mea) {
            if(s.schets.Verwijder(p)) {
                s.schets.Herteken();
                s.Invalidate();
            }
        }
        public string ToSVG() { return ""; }
    }

    public class PickColor : Startpunt
    {
        public override void MuisDrag(SchetsControl s, Point p, MouseEventArgs mea) { }

        public override void MuisLos(SchetsControl s, Point p, MouseEventArgs mea)
        {
            Color picked = ((Bitmap)s.schets.image).GetPixel(p.X, p.Y);
            if (mea.Button == MouseButtons.Left)
            {
                s.penkleur1 = picked;
                ((SchetsWin)s.Parent).colorBox1.BackColor = picked;
            }
            else
            {
                s.penkleur2 = picked;
                ((SchetsWin)s.Parent).colorBox2.BackColor = picked;
            }
            s.Invalidate();
        }

        public override void Teken(Graphics g) { }
        public override void Letter(SchetsControl s, char c) { }
        public override bool Bevat(Point p) {return true;}
        public override string ToSVG() { return ""; }
    }

    public class Plaatje : Vlak {
        Bitmap bmp;

        public Plaatje(Bitmap b) {
            bmp = b;
            startpunt = new Point(0, 0);
            eindpunt = new Point(b.Width, b.Height);
        }

        public override void MuisLos(SchetsControl s, Point p, MouseEventArgs mea) {
            base.MuisLos(s, p, mea);
            if(bmp == null) {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.FileOk += delegate {
                    bmp = (Bitmap)Bitmap.FromFile(ofd.FileName);
                };
                ofd.ShowDialog();
            }
        }

        public override void Teken(Graphics g) {
            g.DrawImage(bmp, Punten2Rechthoek(startpunt, eindpunt));
        }
        public override string ToSVG()
        {
            String resultaat = "<image " + 
                "x=\"" + startpunt.X + "\" "  +
                "y=\"" + startpunt.Y + "\" "  +
                "width=\"" + bmp.Width + "\" " +
                "height=\"" + bmp.Height + "\" " +
                "xlink:href=\"data:image/png;base64,";

            MemoryStream s = new MemoryStream();
            bmp.Save(s, System.Drawing.Imaging.ImageFormat.Png);

            byte[] buf = s.ToArray();

            resultaat += Convert.ToBase64String(buf, 0, buf.Length);

            return resultaat + "\"/>\n";
        }
    }
}
