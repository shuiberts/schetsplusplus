﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace SchetsEditor

{
    public class SchetsControl : UserControl
    {
        public Schets schets;
        public Color penkleur1;
        public Color penkleur2;
        Form colorChooser;
        public PictureBox colorPicker, picked_Color;
        public Label a, r, g, b;
        public int colorA, colorR, colorG, colorB;
        public VScrollBar alpha;
        Bitmap spectrum;

        public SchetsControl()
        {   this.BorderStyle = BorderStyle.Fixed3D;
            this.schets = new Schets();
            this.Paint += this.teken;
            this.MouseEnter += changeCursor;
            this.Resize += this.veranderAfmeting;
            this.veranderAfmeting(null, null);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }
        private void teken(object o, PaintEventArgs pea)
        {   schets.Teken(pea.Graphics);
        }

        public static Cursor CreateCursor(Bitmap bmp, int xHotSpot, int yHotSpot)
        {
            return new Cursor(bmp.GetHicon());
        }


                public void changeCursor(object o, EventArgs e)
                {
                    Cursor myCursor = new Cursor(new System.IO.MemoryStream(Properties.Resources.Cursor_Draw));

                    string currentTool = ((SchetsWin)Parent).HuidigeTool.ToString();
                    
                    switch (((Control)o).Parent.Name.ToString())
                    {
                        case "paneel":
                        case "SchetsEditor":
                            switch (currentTool)
                            {
                                default:
                                    myCursor = new Cursor(new System.IO.MemoryStream(
                            Properties.Resources.Cursor_Draw));
                                    break;
                                case "PenStreep":
                                    myCursor = new Cursor(new System.IO.MemoryStream(
                                                      Properties.Resources.Cursor_Pen));
                                    break;
                                case "Gum":
                                        myCursor = createEraserCursor(
                                ((SchetsWin)Parent).SizeBar.Value * 4);
                                    break;
                                case "PickColor":
                                    myCursor = new Cursor(new System.IO.MemoryStream(
                                                      Properties.Resources.Pick_Color));
                                    break;
                            }
                            o = this;
                            break;
                        case "ColorChooser":
                            myCursor = new Cursor(new System.IO.MemoryStream(
                                                      Properties.Resources.Cursor_Draw));
                            break;
                    }
                    ((Control)o).Cursor = myCursor;
                }

        //Create EraserCursor neem size > 1 voor zichtbare cursor;
        public Cursor createEraserCursor(int size)
        {
            if (size < 1)
                size = 1;
            Bitmap eraser = new Bitmap(size,size);
            Graphics g = Graphics.FromImage(eraser);
            Rectangle Gum = new Rectangle(0, 0, size - 1, size - 1);
            g.FillRectangle(Brushes.White, Gum);
            g.DrawRectangle(Pens.Black, Gum);
            return CreateCursor(eraser, size/2, size/2);
        }

        private void veranderAfmeting(object o, EventArgs ea)
        {   schets.VeranderAfmeting(this.ClientSize);
            this.Invalidate();
        }
        public Graphics MaakBitmapGraphics()
        {   Graphics g = schets.BitmapGraphics;
            return g;
        }
        public void Schoon(object o, EventArgs ea)
        {   schets.elementen = new System.Collections.Generic.LinkedList<ISchetsTool>();
            schets.Schoon();
            this.Invalidate();
        }
        public void Roteer(object o, EventArgs ea)
        {   schets.Roteer();
            this.veranderAfmeting(o, ea);
        }
        public void PickColor(object obj, EventArgs ea)
        {
            colorR = penkleur1.R;
            colorG = penkleur1.G;
            colorB = penkleur1.B;
            spectrum = ColorPickSpectrum(180, 180);

            colorChooser = new Form();
            colorChooser.ClientSize = new Size(288, 180);
            colorChooser.Text = "ColorPicker";
            colorChooser.Name = "ColorChooser";
            colorChooser.ShowInTaskbar = false;
            colorChooser.BackColor = Color.Gray;
            colorChooser.Icon = Properties.Resources.settings;
            colorChooser.StartPosition = FormStartPosition.CenterParent;
            colorChooser.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;

            alpha = new VScrollBar();
            alpha.Location = new Point(180, 0);
            alpha.Height = 180;
            alpha.Width = 30;
            alpha.Minimum = -50;
            alpha.Maximum = 59;
            alpha.Value = 0;
            colorChooser.Controls.Add(alpha);
            alpha.ValueChanged += alpha_Changed;

            colorPicker = new PictureBox();
            colorPicker.Location = new Point(0, 0);
            colorPicker.Size = new Size(180, 180);
            colorPicker.BackgroundImage = spectrum;
            colorChooser.Controls.Add(colorPicker);
            colorPicker.MouseEnter += changeCursor;
            colorPicker.MouseMove += moveOnColor;
            colorPicker.MouseLeave += changeCursor;
            colorPicker.MouseClick += changeColor;

            picked_Color = new PictureBox();
            picked_Color.Location = new Point(213, 103);
            picked_Color.Size = new Size(72, 26);
            picked_Color.Image = borderLines(picked_Color.Size);
            picked_Color.BackColor = penkleur1;
            colorChooser.Controls.Add(picked_Color);

            Button bt = new Button();
            bt.Location = new Point(210, 130);
            bt.Text = "Set Color1";
            bt.Width = 78;
            bt.Height = 25;
            bt.BackColor = Color.Gainsboro;
            colorChooser.Controls.Add(bt);
            bt.Click += delegate(object sender, EventArgs e) { set_Color(sender, e, 1, picked_Color.BackColor); };

            bt = new Button();
            bt.Location = new Point(210, 155);
            bt.Text = "Set Color2";
            bt.Width = 78;
            bt.Height = 25;
            bt.BackColor = Color.Gainsboro;
            colorChooser.Controls.Add(bt);
            bt.Click += delegate(object sender, EventArgs e) { set_Color(sender, e, 2, picked_Color.BackColor); };

            bt = new Button();
            bt.Location = new Point(210, 79);
            bt.Text = "Reset A";
            bt.Width = 60;
            bt.BackColor = Color.Gainsboro;
            colorChooser.Controls.Add(bt);
            bt.Click += reset_Alpha;

            a = new Label();
            a.Text = alpha.Value.ToString();
            a.Location = new Point(210, 3);
            a.Text = "A:" + (50 - alpha.Value) + "%";
            colorChooser.Controls.Add(a);

            r = new Label();
            r.Text = alpha.Value.ToString();
            r.Location = new Point(210, 23);
            r.Text = "R:" + colorR;
            colorChooser.Controls.Add(r);

            g = new Label();
            g.Text = alpha.Value.ToString();
            g.Location = new Point(210, 43);
            g.Text = "G:" + colorG;
            colorChooser.Controls.Add(g);

            b = new Label();
            b.Text = alpha.Value.ToString();
            b.Location = new Point(210, 63);
            b.Text = "B:" + colorB;
            colorChooser.Controls.Add(b);
            

            colorChooser.ShowDialog();
        }

        public void moveOnColor(object o, MouseEventArgs me)
        {
            if ((Control.MouseButtons & MouseButtons.Left) != 0)
                changeColor(o, me);
        }

        public void changeColor(object o, EventArgs e)
        {
            int x = onInterval(0, 179, colorPicker.PointToClient(MousePosition).X);
            int y = onInterval(0, 179, colorPicker.PointToClient(MousePosition).Y);

            colorR = spectrum.GetPixel(x,y).R;
            colorG = spectrum.GetPixel(x,y).G;
            colorB = spectrum.GetPixel(x,y).B;

            alpha_Changed(o, e);
        }

        public void reset_Alpha(object o, EventArgs e)
        {
            alpha.Value = 0;
            alpha_Changed(o, e);
        }

        public void alpha_Changed(object o, EventArgs e)
        {
            a.Text = "A:" + (50 - alpha.Value) + "%";

            colorA = alpha.Value;
            int aColorR = onInterval(0, 255, (int)(colorR - Math.Sign(colorA) *
                                            (127.0 + Math.Abs(127.0 - colorR)) / 49.0 * Math.Abs(colorA)));
            int aColorG = onInterval(0, 255, (int)(colorG - Math.Sign(colorA) *
                                            (127.0 + Math.Abs(127.0 - colorG)) / 49.0 * Math.Abs(colorA)));
            int aColorB = onInterval(0, 255, (int)(colorB - Math.Sign(colorA) *
                                            (127.0 + Math.Abs(127.0 - colorB)) / 49.0 * Math.Abs(colorA)));
            r.Text = "R:" + aColorR;
            g.Text = "G:" + aColorG;
            b.Text = "B:" + aColorB;

            picked_Color.BackColor = Color.FromArgb(aColorR, aColorG, aColorB);
        }


        public void set_Color(object o, EventArgs e, int colorNumber, Color new_Color)
        {
            if (colorNumber == 1)
            {
                penkleur1 = new_Color;
                ((SchetsWin)Parent).colorBox1.BackColor = new_Color;
            }

            if (colorNumber == 2)
            {
                penkleur2 = new_Color;
                ((SchetsWin)Parent).colorBox2.BackColor = new_Color;
            }
        }

        private int onInterval(int min, int max, int v)
        {
            int result;
            if (v <= max)
                if (v >= min)
                    result = v;
                else
                    result = min;
            else
                result = max;
            return result;
        }


        public Bitmap ColorPickSpectrum(double width, double height)
        {
            // |||||||||Color Picker Spectrum|||||||||
            Bitmap spectrum = new Bitmap((int)width, (int)height);
            // Pixelbreedte Kleurkolom p, RGB Coefficient 255/Pixelbreedte Kleurkolom d
            double p = width / 6, d = 255 / p;
            // Aantal Kleurkolommen is 6
            int[] kleurWaarden = new int[6];


            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    double dy = y;
                    kleurWaarden[1] = (int)(255 - d * (x % p) + (127 - (255 - d * (x % p))) * (height - dy) / height);
                    kleurWaarden[2] = (int)(0 + 127 * (height - dy) / height);
                    kleurWaarden[3] = (int)(0 + 127 * (height - dy) / height);
                    kleurWaarden[4] = (int)(d * (x % p) + (127 - d * (x % p)) * (height - dy) / height);
                    kleurWaarden[5] = (int)(255 + (127 - 255) * (height - dy) / height);
                    kleurWaarden[0] = (int)(255 + (127 - 255) * (height - dy) / height);

                    for (int k = 0; k < 6; k++)
                    {
                        if (x >= k * p && x < (k + 1) * p)
                        {
                            spectrum.SetPixel((int)height - (x + 1), (int)height - (y + 1),
                                                       Color.FromArgb(kleurWaarden[k % 6],
                                                                      kleurWaarden[(k + 2) % 6],
                                                                      kleurWaarden[(k + 4) % 6]));
                        }
                    }
                }

            }
            return spectrum;

        }

        public Bitmap borderLines(Size imgSize)
        {
            Bitmap bmp = new Bitmap(imgSize.Width, imgSize.Height);
            Graphics g = Graphics.FromImage(bmp);
            if (imgSize.Width > 0 && imgSize.Height > 0)
                g.DrawRectangle(Pens.Black, 0, 0, imgSize.Width - 1, imgSize.Height - 1);
            return bmp;
        }


        public void Laad(String filename) {
            schets = new Schets(filename);
            schets.VeranderAfmeting(this.ClientSize);
            schets.Herteken();
            this.Invalidate();
        }
    }
}
