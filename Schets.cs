﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

namespace SchetsEditor
{
    public class Schets
    {
        private Bitmap bitmap;
        public LinkedList<ISchetsTool> elementen = new LinkedList<ISchetsTool>();
        
        public Image image {
            get {
                return bitmap;
            }
        }
        
        public Schets()
        {
            bitmap = new Bitmap(1, 1);
        }
        public Schets(String filename)
        {
            // We kunnen alleen onze eigen svgtjes laden!!
            if(filename.EndsWith(".svg", StringComparison.CurrentCultureIgnoreCase)) {
                StreamReader file = new StreamReader(filename);
                String line;

                // First 5 lines are headers and such
                // I know, it is not very elegant
                for(int i = 0; i < 5; i++)
                    file.ReadLine();
                
                bitmap = new Bitmap(1, 1);

                while((line = file.ReadLine()) != null) {

                    // Are we there yet?
                    if(line.Substring(0, 5) == "</svg")
                        return;

                    if(line.IndexOf(' ') == -1)
                        Console.WriteLine("No space in \"" + line + "\"");

                    switch(line.Substring(1, line.IndexOf(' ') - 1)) {
                        case "rect":
                            if(line.Contains("fill-opacity:0"))
                                elementen.AddLast(new Rechthoek(line));
                            else
                                elementen.AddLast(new Vlak(line));
                            break;
                        case "ellipse":
                            if(line.Contains("fill-opacity:0"))
                                elementen.AddLast(new Cirkel(line));
                            else
                                elementen.AddLast(new VolCirkel(line));
                            break;
                        case "line":
                            elementen.AddLast(new Lijn(line));
                            break;
                        case "path":
                            elementen.AddLast(new PenStreep(line));
                            break;
                        case "text":
                            elementen.AddLast(new Tekst(line));
                            break;
                        default:
                            throw new NotImplementedException(line.Substring(0, line.IndexOf(' ')));
                    }
                }

                file.Close();
            } else {
                Bitmap import = (Bitmap)Bitmap.FromFile(filename);
                bitmap = new Bitmap(import.Width, import.Height);
                Plaatje p = new Plaatje(import);
                elementen.AddLast(p);
                p.Teken(BitmapGraphics);
            }
        }
        public Graphics BitmapGraphics
        {
            get {
                Graphics g = Graphics.FromImage(bitmap);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                return g;
            }
        }
        public void VeranderAfmeting(Size sz)
        {
            Bitmap nieuw = new Bitmap(sz.Width, sz.Height);
            Graphics gr = Graphics.FromImage(nieuw);
            gr.FillRectangle(Brushes.White, 0, 0, sz.Width, sz.Height);
            gr.DrawImage(bitmap, 0, 0);
            bitmap = nieuw;
        }
        public void Teken(Graphics gr)
        {
            gr.DrawImage(bitmap, 0, 0);
        }
        public void Schoon()
        {
            Graphics gr = Graphics.FromImage(bitmap);
            gr.FillRectangle(Brushes.White, 0, 0, bitmap.Width, bitmap.Height);
        }

        int rotatie = 0;
        public void Roteer()
        {
            rotatie = (rotatie + 90) % 360;
            LinkedListNode<ISchetsTool> node = elementen.First;
            if(node != null) {
                do {
                    node.Value.Roteer(bitmap.Width / 2, bitmap.Height / 2, rotatie);
                } while((node = node.Next) != null);
            }
            Herteken();
        }

        public void VoegToe(ISchetsTool tool) {
            if(tool.ToString() != "Gum") {
                elementen.AddLast(tool);
            }
        }
        public bool Verwijder(Point p) {
            LinkedListNode<ISchetsTool> element = elementen.Last;
            while(element != null) {
                if(element.Value.Bevat(p)) {
                    elementen.Remove(element);
                    return true;
                } else
                    element = element.Previous;
            }
            return false;
        }
        public void Herteken() {
            Schoon();

            LinkedListNode<ISchetsTool> element = elementen.First;
            while(element != null) {
                element.Value.Teken(BitmapGraphics);

                element = element.Next;
            }
        }

        /*
         * Merk op: we implementeren geen goede svg hier, maar slechts
         * een subset er van, en negeren ook een paar regeltjes van xml.
         * Zo verwachten we dat er op iedere lijn slechts 1 element staat,
         * en dat ze een vast patroon volgen. We werken dus niet met SVG
         * om een SVG editor te maken, maar zodat je, in het geval dat je
         * per ongeluk iets moois maakt, dat bestand kunt openen in een
         * betere editor, om er iets serieus mee te doen.
         * 
         * TLDR; Compatibiliteit is eenrichtingsverkeer, alleen van deze
         * editor naar andere SVG editors, niet andersom.
         */
        public void Save(String filename) {
            FileStream s = new FileStream(filename, FileMode.Create);

            LinkedListNode<ISchetsTool> element = elementen.First;

            // SVG header
            byte[] buf = Encoding.ASCII.GetBytes(
                "<?xml version=\"1.0\" standalone=\"no\"?>\n" +
                "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n" + 
                "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n" +
                "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">" +
                "\n" +
                "\n");
            s.Write(buf, 0, buf.Length);

            String str;
            while(element != null) {
                str = element.Value.ToSVG();
                buf = Encoding.ASCII.GetBytes(str);
                s.Write(buf, 0, buf.Length);
                element = element.Next;
            }

            buf = Encoding.ASCII.GetBytes("</svg>\n");
            s.Write(buf, 0, buf.Length);

            s.Close();
        }
    }
}
